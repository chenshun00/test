package io.github.chenshun00.gateway.test.dao;

/**
 * @author chenshun00@gmail.com
 * @since 2019-08-29 18:26
 */
public class User {
    Integer id;
    String username;
    String password;
    Integer status;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
