//package top.huzhurong.gateway.test.log;
//
//import org.apache.log4j.xml.DOMConfigurator;
//import org.springframework.boot.logging.LogFile;
//import org.springframework.boot.logging.LoggingInitializationContext;
//import org.springframework.boot.logging.Slf4JLoggingSystem;
//import org.springframework.util.ResourceUtils;
//
//import java.io.FileNotFoundException;
//import java.net.URL;
//
///**
// * @author chenshun00@gmail.com
// * @since 2019-09-24 13:44
// */
//public class Log4jLoggingSystem extends Slf4JLoggingSystem {
//
//    public Log4jLoggingSystem(ClassLoader classLoader) {
//        super(classLoader);
//    }
//
//
//    @Override
//    public void initialize(LoggingInitializationContext initializationContext, String configLocation, LogFile logFile) {
//        super.initialize(initializationContext, configLocation, logFile);
//    }
//
//    @Override
//    protected void loadConfiguration(LoggingInitializationContext initializationContext, String location, LogFile logFile) {
//        try {
//            URL url = ResourceUtils.getURL(initializationContext.getEnvironment().getProperty("logging.config"));
//            DOMConfigurator.configure(url);
//        } catch (FileNotFoundException e) {
//            throw new IllegalArgumentException(e);
//        }
//    }
//
//    @Override
//    protected String[] getStandardConfigLocations() {
//        return new String[]{"log4j.xml", "log4j-prod.xml", "log4j-test.xml", "log4j-dev.xml"};
//    }
//
//    @Override
//    protected void loadDefaults(LoggingInitializationContext initializationContext, LogFile logFile) {
//        try {
//            URL url = ResourceUtils.getURL(initializationContext.getEnvironment().getProperty("logging.config"));
//            DOMConfigurator.configure(url);
//        } catch (FileNotFoundException e) {
//            throw new IllegalArgumentException(e);
//        }
//    }
//}
