//package top.huzhurong.gateway.test.command;
//
//import org.springframework.shell.standard.ShellComponent;
//import org.springframework.shell.standard.ShellMethod;
//import org.springframework.shell.standard.ShellOption;
//
///**
// * @author chenshun00@gmail.com
// * @since 2019-09-04 22:53
// */
////@ShellComponent
//public class TranslationCommands {
//    @ShellMethod("Add two integers together.")
//    public int add(int a, int b) {
//        return a + b;
//    }
//
//    @ShellMethod("please input your name")
//    public String your(@ShellOption(value = {"-n", "--name"}, help = "输入你的名字") String name,
//                       @ShellOption(value = {"-a", "--age"}, help = "年纪", defaultValue = "20") Integer age) {
//        return "name:" + name + "\tage:" + age;
//    }
//}
