package io.github.chenshun00.gateway.test.annotation;

import java.lang.annotation.*;

/**
 * 自定义注入
 *
 * @author chenshun00@gmail.com
 * @since 2019-09-02 22:12
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface CustomInject {
    /**
     * 测绘
     * Declares whether the annotated dependency is required.
     * <p>Defaults to {@code true}.
     */
    boolean required() default false;
}
