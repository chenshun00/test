package io.github.chenshun00.gateway.test.rpc;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;

import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author chenshun00@gmail.com
 * @since 2019-10-19 16:13
 */
public class MessageWrite {
    public static void main(String[] args) throws Exception {
        //RandomAccessFile randomAccessFile = new RandomAccessFile("0000000000000", "rw");
        //FileChannel channel = randomAccessFile.getChannel();
        //MappedByteBuffer map = channel.map(FileChannel.MapMode.READ_WRITE, 0, 1024 * 2024 * 50);
        ByteBuffer map = ByteBuffer.allocate(1024);
        AtomicInteger at = new AtomicInteger(0);

        {
            ByteBuffer byteBuffer = map.slice();
            byteBuffer.position(at.get());
            Message message = new Message();
            message.setBody("我为人人,人人为我".getBytes());
            message.setTopic("chenshun00");
            message.setProperties(new HashMap<String, String>() {{
                this.put("11", "11");
                this.put("22", "22");
                this.put("33", "33");
            }});
            at.set(writeMessage(message, byteBuffer));
        }

        {
            ByteBuffer byteBuffer = map.slice();
            byteBuffer.position(at.get());
            Message message = new Message();
            message.setBody("我为人人2,人人为我2".getBytes());
            message.setTopic("chenshun00");
            message.setProperties(new HashMap<String, String>() {{
                this.put("44", "77");
                this.put("55", "88");
                this.put("66", "99");
            }});
            at.set(writeMessage(message, byteBuffer));
        }
        //map.flip();
        {
            Message read = read(map);
            System.out.println(read.id);
            System.out.println(read.getProperties().toString());
            System.out.println(read.getTopic());
            System.out.println(new String(read.getBody()));
        }
        {
            Message read = read(map);
            System.out.println(read.id);
            System.out.println(read.getProperties().toString());
            System.out.println(read.getTopic());
            System.out.println(new String(read.getBody()));
        }
    }


    public static int writeMessage(Message message, ByteBuffer byteBuffer) {
        //id 8
        //topic n 长度
        //properties n 长度
        //body n 长度
        int result = 0;
        String topic = message.getTopic();
        byte[] topicByte = topic.getBytes();
        byteBuffer.putLong(message.id);
        result += 8;
        byteBuffer.putInt(topicByte.length);
        result += 4;
        byteBuffer.put(topicByte, 0, topicByte.length);
        result += topicByte.length;
        Map<String, String> properties = message.getProperties();
        int length = 0;
        byte[] propertiesByte = null;
        if (properties != null) {
            String toStr = toStr(properties);
            propertiesByte = toStr.getBytes();
            length = propertiesByte.length;
        }
        //转成字符串
        byteBuffer.putInt(length);
        result += 4;
        if (length > 0 && propertiesByte != null) {
            byteBuffer.put(propertiesByte, 0, propertiesByte.length);
            result += propertiesByte.length;
        }
        byte[] body = message.getBody();
        byteBuffer.putInt(body.length);
        result += 4;
        byteBuffer.put(body, 0, body.length);
        result += body.length;
        return result;
    }

    public static Message read(ByteBuffer byteBuffer) {
        Message message = new Message();
        message.id = byteBuffer.getLong();
        {
            int anInt = byteBuffer.getInt();
            byte[] topicByte = new byte[anInt];
            byteBuffer.get(topicByte, 0, anInt);
            message.setTopic(new String(topicByte));
        }

        {
            int properties = byteBuffer.getInt();
            byte[] propertiesByte = new byte[properties];
            byteBuffer.get(propertiesByte, 0, properties);
            message.setProperties(toMap(new String(propertiesByte)));
        }

        {
            int properties = byteBuffer.getInt();
            byte[] propertiesByte = new byte[properties];
            byteBuffer.get(propertiesByte, 0, properties);
            message.setBody(propertiesByte);
        }
        return message;
    }

    private static String toStr(Map<String, String> xx) {
        return JSONObject.toJSONString(xx);
    }

    private static Map<String, String> toMap(String json) {
        return JSON.parseObject(json, new TypeReference<HashMap<String, String>>() {
        });
    }

}
