package io.github.chenshun00.gateway.test.annotation;

import org.springframework.context.annotation.Import;
import io.github.chenshun00.gateway.test.post.RunThreeConfiguration;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Import(RunThreeConfiguration.class)
public @interface EnableRunThree {
}
