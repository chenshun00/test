package io.github.chenshun00.gateway.test;

import io.github.chenshun00.gateway.test.annotation.EnableRunThree;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.elasticsearch.ElasticsearchDataAutoConfiguration;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.Bean;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

@SpringBootApplication(exclude = {ElasticsearchDataAutoConfiguration.class})
@EnableRunThree
@ServletComponentScan
public class TestApplication {

    public static void main(String[] args) {
        System.setProperty("spring.profiles.active", "test");
//        System.setProperty("org.springframework.boot.logging.LoggingSystem", "top.huzhurong.gateway.test.log.Log4jLoggingSystem");
        SpringApplication.run(TestApplication.class, args);
    }

    @Bean(destroyMethod = "shutdown")
    public Executor taskScheduler() {
        return Executors.newScheduledThreadPool(10);
    }


}
