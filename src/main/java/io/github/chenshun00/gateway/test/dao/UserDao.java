package io.github.chenshun00.gateway.test.dao;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author chenshun00@gmail.com
 * @since 2019-08-29 18:02
 */
@Repository
public class UserDao {

    @Autowired
    private SqlSessionTemplate sqlSessionTemplate;

    public List<User> selectAll() {
        return sqlSessionTemplate.selectList("User.se");

    }

    public String insert() {
        User user = new User();
        user.setPassword("111");
        user.setUsername("cccc");
        user.setStatus(1);
        sqlSessionTemplate.insert("User.insert", user);
        return user.getUsername();
    }
}
