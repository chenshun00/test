package io.github.chenshun00.gateway.test.buffer;

import java.nio.ByteBuffer;

/**
 * @author chenshun00@gmail.com
 * @since 2019-10-19 17:00
 */
public class DuplicateTest {
    public static void main(String[] args) {
        ByteBuffer allocate = ByteBuffer.allocate(16);
        allocate.putInt(1);
        ByteBuffer duplicate = allocate.slice();//复制buffer
        allocate.putInt(2);
        duplicate.flip();
        System.out.println(duplicate.getInt());//输出1
        System.out.println(duplicate.getInt());//输出2
    }
}
