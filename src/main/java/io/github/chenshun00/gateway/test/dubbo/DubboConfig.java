package io.github.chenshun00.gateway.test.dubbo;

import com.alibaba.dubbo.config.ProtocolConfig;
import com.google.common.collect.Maps;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;

/**
 * @author chenshun00@gmail.com
 * @since 2019-09-12 22:12
 */
@Configuration
public class DubboConfig {

//    @Bean
//    public ProtocolConfig dubbo() {
//        ProtocolConfig protocolConfig = new ProtocolConfig();
//        protocolConfig.setId("dubbo");
//        protocolConfig.setPort(30090);
//        //todo 这里千万别设置，否则报错
//        //protocolConfig.setHost("119.23.34.45");
//        System.setProperty("DUBBO_IP_TO_REGISTRY", "119.23.34.45");
//        //todo 这里的端口优先级更高
//        System.setProperty("dubbo.protocol.port", "30009");
//        HashMap<String, String> objectObjectHashMap = Maps.newHashMap();
//        objectObjectHashMap.put("anyhost", "true");
//        protocolConfig.setParameters(objectObjectHashMap);
//        return protocolConfig;
//    }

}
