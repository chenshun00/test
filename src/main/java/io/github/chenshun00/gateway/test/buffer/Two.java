package io.github.chenshun00.gateway.test.buffer;

import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author chenshun00@gmail.com
 * @since 2019-10-18 21:51
 */
public class Two {
    public static void main(String[] args) throws Exception {
        RandomAccessFile randomAccessFile = new RandomAccessFile("inf.txt", "rw");
        FileChannel channel = randomAccessFile.getChannel();
        MappedByteBuffer map = channel.map(FileChannel.MapMode.READ_WRITE, 0, 64);
        AtomicInteger at = new AtomicInteger(0);
        byte[] bytes = new byte[8];
        map.get(bytes, 0, 8);
        System.out.println(new String(bytes));

        bytes = new byte[8];
        map.get(bytes, 0, 8);
        System.out.println(new String(bytes));

//        ByteBuffer allocate = ByteBuffer.allocate(16);
//        allocate.putInt(1);
//        allocate.putInt(2);
//        allocate.flip();
//        System.out.println("1");
//        ByteBuffer slice = allocate.slice();
//        slice.position(0);
//        slice.putLong(1L);
//
//        allocate.slice();
//        slice.position(8);
//        slice.putLong(2L);
//
//        long aLong = allocate.getLong();
//        System.out.println(aLong);
//        aLong = allocate.getLong();
//        System.out.println(aLong);
    }
}
