package io.github.chenshun00.gateway.test.hash;

/**
 * @author chenshun00@gmail.com
 * @since 2019-10-17 17:23
 */
public class ClientNode implements Node {
    private final String clientID;

    public ClientNode(String clientID) {
        this.clientID = clientID;
    }

    @Override
    public String getKey() {
        return clientID;
    }
}

