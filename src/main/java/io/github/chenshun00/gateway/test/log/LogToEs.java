//package top.huzhurong.gateway.test.log;
//
//import java.io.IOException;
//import java.io.OutputStream;
//
///**
// * @author chenshun00@gmail.com
// * @since 2019-09-04 10:36
// */
//public enum LogToEs {
//    SystemOut("System.out", new OutputStream() {
//        @Override
//        public void write(int b) throws IOException {
//            System.out.write(1);
//        }
//
//        //入队列然后直接发送到mq等等. mq写入到es中去
//        @Override
//        public void write(byte b[]) throws IOException {
//            System.out.write(b);
//        }
//
//        @Override
//        public void write(byte b[], int off, int len) throws IOException {
//            System.out.write(b, off, len);
//        }
//
//        @Override
//        public void flush() throws IOException {
//            System.out.flush();
//        }
//    }),
//
//    SystemErr("System.err", new OutputStream() {
//        @Override
//        public void write(int b) throws IOException {
//            System.err.write(b);
//        }
//
//        @Override
//        public void write(byte b[]) throws IOException {
//            System.err.write(b);
//        }
//
//        @Override
//        public void write(byte b[], int off, int len) throws IOException {
//            System.err.write(b, off, len);
//        }
//
//        @Override
//        public void flush() throws IOException {
//            System.err.flush();
//        }
//    });
//
//    public static LogToEs findByName(String name) {
//        for (LogToEs target : LogToEs.values()) {
//            if (target.name.equalsIgnoreCase(name)) {
//                return target;
//            }
//        }
//        return null;
//    }
//
//    private final String name;
//    private final OutputStream stream;
//
//    private LogToEs(String name, OutputStream stream) {
//        this.name = name;
//        this.stream = stream;
//    }
//
//    public String getName() {
//        return name;
//    }
//
//    public OutputStream getStream() {
//        return stream;
//    }
//
//    @Override
//    public String toString() {
//        return name;
//    }
//}
