package io.github.chenshun00.gateway.test.buffer;

import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author chenshun00@gmail.com
 * @since 2019-10-18 20:39
 */
public class ByteBufTest {
    public static void main(String[] args) throws Exception {
        RandomAccessFile randomAccessFile = new RandomAccessFile("inf.txt", "rw");
        FileChannel channel = randomAccessFile.getChannel();
        MappedByteBuffer map = channel.map(FileChannel.MapMode.READ_WRITE, 0, 64);
        AtomicInteger at = new AtomicInteger(0);

        ByteBuffer byteBuffer = map.slice();
        byteBuffer.position(at.get());
        byteBuffer.put("chenshun".getBytes(), 0, 8);
        at.addAndGet(8);

        byteBuffer = map.slice();
        byteBuffer.position(at.get());
        byteBuffer.put("chenshun".getBytes(), 0, 8);
        at.addAndGet(8);

        byteBuffer = map.slice();
        byteBuffer.position(at.get());
        byteBuffer.put("chenshun".getBytes(), 0, 8);
        at.addAndGet(8);
        byteBuffer = map.slice();
        byteBuffer.position(at.get());
        byteBuffer.put("chenshun".getBytes(), 0, 8);
        at.addAndGet(8);
        byteBuffer = map.slice();
        byteBuffer.position(at.get());
        byteBuffer.put("chenshun".getBytes(), 0, 8);
        at.addAndGet(8);
        byteBuffer = map.slice();
        byteBuffer.position(at.get());
        byteBuffer.put("chenshun".getBytes(), 0, 8);
        at.addAndGet(8);
        byteBuffer = map.slice();
        byteBuffer.position(at.get());
        byteBuffer.put("chenshun".getBytes(), 0, 8);
        at.addAndGet(8);
        byteBuffer = map.slice();
        byteBuffer.position(at.get());
        byteBuffer.put("chenshun".getBytes(), 0, 8);
        at.addAndGet(8);
        System.out.println(at.get());
        map.force();
    }

    public static void slice(ByteBuffer byteBuffer) {
        //切片出来的buffer的limit caps 和position都是原来剩下来的
        ByteBuffer slice = byteBuffer.slice();
        slice.position(byteBuffer.position());
        slice.putInt(11);
        slice.putInt(22);
        slice.putInt(33);
        //slice.putInt(44);
    }

    public static void out(ByteBuffer byteBuffer) {
        byteBuffer.flip();
        int ff = byteBuffer.getInt();
        int ss = byteBuffer.getInt();
        int tt = byteBuffer.getInt();
        int f4 = byteBuffer.getInt();
        int f5 = byteBuffer.getInt();
        int f6 = byteBuffer.getInt();
        int f7 = byteBuffer.getInt();
        int f8 = byteBuffer.getInt();
        System.out.println("ff:" + ff);
        System.out.println("ss:" + ss);
        System.out.println("tt:" + tt);
        System.out.println("f4:" + f4);
        System.out.println("f5:" + f5);
        System.out.println("f6:" + f6);
        System.out.println("f7:" + f7);
        System.out.println("f8:" + f8);
    }
}
