package io.github.chenshun00.gateway.test.hash;

import java.util.Arrays;
import java.util.HashMap;

/**
 * @author chenshun00@gmail.com
 * @since 2019-10-17 17:25
 */
public class Test {
    protected static HashMap<String/* topic-queueid */, Long/* offset */> topicQueueTable = new HashMap<String, Long>(1024);
    public static void main(String[] args) {
        ClientNode ff = new ClientNode("1");
        ClientNode ss = new ClientNode("2");
        ClientNode tt = new ClientNode("3");
        ClientNode f4 = new ClientNode("4");

        ConsistentHashRouter router = new ConsistentHashRouter(Arrays.asList(ff, ss, tt, f4), 100);
        Node chenshun = router.routeNode("chenshun");
        System.out.println(chenshun.getKey());
        router.addNode(new ClientNode("5"), 25);
        chenshun = router.routeNode("chenshun");
        System.out.println(chenshun.getKey());
        router.removeNode(tt);
        chenshun = router.routeNode("chenshun");
        System.out.println(chenshun.getKey());

        topicQueueTable.put("22",123123L);

        Long aLong = topicQueueTable.computeIfAbsent("22", ke -> 0L);
        System.out.println(aLong);
    }
}
