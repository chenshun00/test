package io.github.chenshun00.gateway.test.listen;

import lombok.Getter;
import lombok.Setter;
import org.springframework.context.ApplicationEvent;

/**
 * @author chenshun00@gmail.com
 * @since 2019-08-17 23:28
 */
@Getter
@Setter
public class Custom extends ApplicationEvent {
    /**
     * Create a new ApplicationEvent.
     *
     * @param source the object on which the event initially occurred (never {@code null})
     */
    public Custom(Object source, String name) {
        super(source);
        this.name = name;
    }

    private String name;
}
