package io.github.chenshun00.gateway.test.view

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.InitializingBean
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationContext
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import io.github.chenshun00.gateway.test.annotation.CustomInject
import io.github.chenshun00.gateway.test.dao.UserDao
import io.github.chenshun00.gateway.test.listen.Custom
import java.lang.management.ManagementFactory
import javax.management.ObjectName
import javax.servlet.http.HttpServletResponse

@RestController
class KotCtrl/*(private val userDao: UserDao)*/ : InitializingBean {

    private val logger = LoggerFactory.getLogger(KotCtrl::class.java)

    @CustomInject
    private lateinit var userDao: UserDao
    @Autowired
    private lateinit var conext: ApplicationContext

    @RequestMapping("kot")
    fun kotTest(response: HttpServletResponse, name: String) {
        logger.info("[name:{}]", name);
        userDao.insert()
        val printWriter = response.writer
        for (i in 1..101) {
            printWriter.println("这里是一端测试，看能不能在面板上输出出来")
            printWriter.flush()
            Thread.sleep(100L)
        }
    }


    @GetMapping("port")
    fun port(): String {
        val mbeanServer = ManagementFactory.getPlatformMBeanServer()
        val queryNames = mbeanServer.queryNames(ObjectName("Tomcat:type=ThreadPool,name=*"), null)
        var port = "8080"
        queryNames.forEach {
            port = mbeanServer.getAttribute(it, "localPort").toString();
        }
        conext.publishEvent(Custom(this, "kotlin"))
        return port
    }

    override fun afterPropertiesSet() {
        println("afterPropertiesSet")
    }
}