package io.github.chenshun00.gateway.test.post;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Role;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.concurrent.ConcurrentTaskScheduler;
import org.springframework.scheduling.concurrent.CustomizableThreadFactory;

import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * @author chenshun00@gmail.com
 * @since 2019-08-31 22:04
 */
@Configuration
@Role(BeanDefinition.ROLE_INFRASTRUCTURE)
public class RunThreeConfiguration {
    @Bean("annotationPostProcessor")
    @Role(BeanDefinition.ROLE_INFRASTRUCTURE)
    public AnnotationPostProcessor annotationPostProcessor() {
        return new AnnotationPostProcessor();
    }

    @Bean("customInjectBeanPostProcessor")
    @Role(BeanDefinition.ROLE_INFRASTRUCTURE)
    public CustomInjectBeanPostProcessor customInjectBeanPostProcessor() {
        return new CustomInjectBeanPostProcessor();
    }

    @Bean
    public TaskScheduler taskScheduler() {
        ScheduledExecutorService scheduledExecutorService = new ScheduledThreadPoolExecutor(1,
                new CustomizableThreadFactory("chenshun"), new RejectedExecutionHandler() {
            @Override
            public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
                System.out.println("gg:" + r);
            }
        });

        ConcurrentTaskScheduler concurrentTaskScheduler = new ConcurrentTaskScheduler(scheduledExecutorService);
        return concurrentTaskScheduler;
    }
}
