package io.github.chenshun00.gateway.test.listen;

import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

/**
 * @author chenshun00@gmail.com
 * @since 2019-08-17 23:30
 */
@Component
public class CustomListener implements ApplicationListener<Custom> {
    @Override
    public void onApplicationEvent(Custom event) {
        String name = event.getName();
        System.out.println("======事件更新:" + name);
    }
}
