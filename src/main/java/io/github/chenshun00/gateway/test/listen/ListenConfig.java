package io.github.chenshun00.gateway.test.listen;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;

/**
 * @author chenshun00@gmail.com
 * @since 2019-09-04 22:26
 */
@Configuration
public class ListenConfig {


    @EventListener
    public void handle(Custom event) {
        String name = event.getName();
        System.out.println("name:" + name);
    }

}
