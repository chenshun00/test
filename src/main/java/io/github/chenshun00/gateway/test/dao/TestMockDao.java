package io.github.chenshun00.gateway.test.dao;

import org.springframework.stereotype.Repository;

/**
 * @author chenshun00@gmail.com
 * @since 2019-09-07 13:30
 */
@Repository
public class TestMockDao {

    public String good() {
        return "good";
    }

}
