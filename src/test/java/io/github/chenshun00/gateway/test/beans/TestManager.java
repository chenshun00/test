package io.github.chenshun00.gateway.test.beans;

import org.junit.Test;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.zip.CRC32;

/**
 * @author chenshun00@gmail.com
 * @since 2019-10-11 10:48
 */
public class TestManager {
    private final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);

    @Test
    public void name() {
//        propertyChangeSupport.addPropertyChangeListener("chenshun", new PropertyChangeListener() {
//            @Override
//            public void propertyChange(PropertyChangeEvent evt) {
//                System.out.println("11");
//            }
//        });
//        propertyChangeSupport.addPropertyChangeListener("chenshun", new PropertyChangeListener() {
//            @Override
//            public void propertyChange(PropertyChangeEvent evt) {
//                System.out.println("22");
//            }
//        });
//
//        propertyChangeSupport.firePropertyChange("chenshun", "22", "33");
//        propertyChangeSupport.firePropertyChange("chenshun2", "22", "33");
//        PropertyChangeListener[] propertyChangeListeners = propertyChangeSupport.getPropertyChangeListeners();
//        System.out.println(propertyChangeListeners.length);
    }

    @Test
    public void crc() {
        CRC32 crc32 = new CRC32();
        byte[] bytes = "chenshun".getBytes();
        crc32.update(bytes, 0, bytes.length);
        int zz = (int) (crc32.getValue() & 0x7FFFFFFF);
        System.out.println(zz);
    }
}
