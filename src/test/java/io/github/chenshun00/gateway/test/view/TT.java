//package top.huzhurong.gateway.test.view;
//
//import com.google.common.base.Charsets;
//import com.taobao.metamorphosis.Message;
//import com.taobao.metamorphosis.client.MessageSessionFactory;
//import com.taobao.metamorphosis.client.MetaClientConfig;
//import com.taobao.metamorphosis.client.MetaMessageSessionFactory;
//import com.taobao.metamorphosis.client.producer.MessageProducer;
//import com.taobao.metamorphosis.client.producer.SendResult;
//import com.taobao.metamorphosis.exception.MetaClientException;
//import com.taobao.metamorphosis.utils.ZkUtils;
//import org.junit.Test;
//
//import java.io.File;
//import java.util.regex.Matcher;
//import java.util.regex.Pattern;
//
///**
// * @author chenshun00@gmail.com
// * @since 2019-09-05 15:01
// */
//public class TT {
//
////    public static void main(String[] args) throws Exception {
////        File file = new File("/Users/admin/Desktop/1.sql");
////        byte[] bytes = java.nio.file.Files.readAllBytes(file.toPath());
////        String content = new String(bytes, Charsets.UTF_8);
////        content = "SET MODE MYSQL;\n\n" + content;
////        content = content.replaceAll("`", "");
////        content = content.replaceAll("COLLATE.*(?=D)", "");
////        content = content.replaceAll("COMMENT.*'(?=,)", "");
////        content = content.replaceAll("\\).*ENGINE.*(?=;)", ")");
////        content = content.replaceAll("INSERT.*", "");
////        content = content.replaceAll("--.*", "");
////        content = content.replaceAll("LOCK.*", "");
////        content = content.replaceAll("UNLOCK.*", "");
////        content = content.replaceAll("UN.*", "");
////        content = content.replaceAll("SET.*", "");
////        content = content.replaceAll("DROP.*", "");
////        content = content.replaceAll("/\\*.*", "");
////        content = content.replaceAll("DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP", " AS CURRENT_TIMESTAMP");
////
////        content = uniqueKey(content);
////        System.out.println(content);
////    }
//
//    /**
//     * h2的索引名必须全局唯一
//     *
//     * @param content sql建表脚本
//     * @return 替换索引名为全局唯一
//     */
//    private static String uniqueKey(String content) {
//        int inc = 0;
//        Pattern pattern = Pattern.compile("(?<=KEY )(.*?)(?= \\()");
//        Matcher matcher = pattern.matcher(content);
//        StringBuffer sb = new StringBuffer();
//        while (matcher.find()) {
//            matcher.appendReplacement(sb, matcher.group() + inc++);
//        }
//        matcher.appendTail(sb);
//        content = sb.toString();
//        return content;
//    }
//
//
//    public static void main(String[] args) {
//        // send message
//        try {
//            final MetaClientConfig metaClientConfig = new MetaClientConfig();
//            final ZkUtils.ZKConfig zkConfig = new ZkUtils.ZKConfig();
//            zkConfig.zkConnect = "192.168.63.127:2181";
//            metaClientConfig.setZkConfig(zkConfig);
//            metaClientConfig.setServerUrl("meta://192.168.63.127:8123");
//
//            MessageSessionFactory sessionFactory = new MetaMessageSessionFactory(metaClientConfig);
//            final MessageProducer producer = sessionFactory.createProducer();
//
//            for (int i = 0; i < 1000; i++) {
//                final SendResult sendResult = producer.sendMessage(new Message("chenshun00", "hello,MetaQ!".getBytes()));
//                // check result
//                if (!sendResult.isSuccess()) {
//                    System.err.println("Send message failed,error message:" + sendResult.getErrorMessage());
//                } else {
//                    System.out.println("Send message successfully,sent to " + sendResult.getPartition());
//                }
//            }
//        } catch (MetaClientException e) {
//            e.printStackTrace(); //TODO log
//        } catch (InterruptedException e) {
//            Thread.currentThread().interrupt();
//        }
//    }
//
//}
