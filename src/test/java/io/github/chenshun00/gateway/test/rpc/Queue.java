//package top.huzhurong.gateway.test.rpc;
//
//import com.google.gson.Gson;
//import org.junit.Test;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import java.io.IOException;
//import java.io.RandomAccessFile;
//import java.nio.ByteBuffer;
//import java.nio.MappedByteBuffer;
//import java.nio.channels.FileChannel;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.List;
//
///**
// * @author luobo.cs@raycloud.com
// * @since 2020/4/12 3:22 下午
// */
//public class Queue {
//
//    Logger LOGGER = LoggerFactory.getLogger(Queue.class);
//
//    ///Users/admin/store/consumequeue/async-test-x/0/00000000000000000000
//    @Test
//    public void testReadQueue() throws IOException {
//
//        RandomAccessFile randomAccessFile = new RandomAccessFile("/Users/admin/store/consumequeue/async-test-x/2/00000000000000000000", "r");
//        FileChannel channel = randomAccessFile.getChannel();
//
//        MappedByteBuffer map = channel.map(FileChannel.MapMode.READ_ONLY, 0, channel.size());
//        List<Tag> xx = new ArrayList<>(128);
//        Tag read = read(map, true);
//        xx.add(read);
//        while (true) {
//            read = read(map, false);
//            if (read == null) {
//                break;
//            }
//            xx.add(read);
////            System.out.println(read);
//            //可以尝试从commitLog去读取数据
//            try (RandomAccessFile ddd =
//                         new RandomAccessFile(
//                                 "/Users/admin/store/commitlog/00000000000000000000",
//                                 "r");) {
//                FileChannel dc = ddd.getChannel();
//                MappedByteBuffer dm = dc.map(FileChannel.MapMode.READ_ONLY, 0, dc.size());
//                dm.position((int) read.offset);
//                readCommitLog(dm);
//            }
//        }
//        System.out.println(xx.size());
//    }
//
//    public static Tag read(MappedByteBuffer mappedByteBuffer, boolean first) {
//        long offset = mappedByteBuffer.getLong();
//        if (offset == 0 && !first) {
//            return null;
//        }
//        int size = mappedByteBuffer.getInt();
//        long code = mappedByteBuffer.getLong();
//
//        return new Tag(offset, size, code);
//    }
//
//    private void readCommitLog(MappedByteBuffer mappedByteBuffer) {
//        int size = mappedByteBuffer.getInt();
//        LOGGER.debug("Message TotalSize:[{}]", size);
//        int magicCode = mappedByteBuffer.getInt();
//        LOGGER.debug("Message MagicCode:[{}]", magicCode);
//        int bodyRPC = mappedByteBuffer.getInt();
//        LOGGER.debug("Message BodyRPC:[{}]", bodyRPC);
//        int queueId = mappedByteBuffer.getInt();
////        System.out.println("queueId:"+queueId);
//        LOGGER.debug("Message QueueId:[{}]", queueId);
//        int flag = mappedByteBuffer.getInt();
//        LOGGER.debug("Message Flag:[{}]", flag);
//        long queueOffset = mappedByteBuffer.getLong();
//        LOGGER.debug("Message ConsumeQueue Offset:[{}]", queueOffset);
//        long physicalOffset = mappedByteBuffer.getLong();
//        LOGGER.debug("Message CommitLog Offset:[{}]", physicalOffset);
//        int sysFlag = mappedByteBuffer.getInt();
//        LOGGER.debug("Message SysFlag:[{}]", sysFlag);
//        long bornTimestamp = mappedByteBuffer.getLong();
//        LOGGER.debug("Message BornTimestamp:[{}]", new Gson().toJson(new Date(bornTimestamp)));
//        long bornHost = mappedByteBuffer.getLong();
//        LOGGER.debug("Message BornHost:[{}]", bornHost);
//        long storeTimestamp = mappedByteBuffer.getLong();
//        LOGGER.debug("Message BornTimestamp:[{}]", new Gson().toJson(new Date(storeTimestamp)));
//        long storeHost = mappedByteBuffer.getLong();
//        LOGGER.debug("Message BornHost:[{}]", storeHost);
//        int reconsumeTimes = mappedByteBuffer.getInt();
//        LOGGER.debug("Message ReconsumeTimes:[{}]", reconsumeTimes);
//        long prepareTransactionOffset = mappedByteBuffer.getLong();
//        LOGGER.debug("Message PrepareTransactionOffset:[{}]", prepareTransactionOffset);
//        int bodyLength = mappedByteBuffer.getInt();
//        LOGGER.debug("Message BodyLength:[{}]", bodyLength);
//        byte[] body = new byte[bodyLength];
//        mappedByteBuffer.get(body);
//        LOGGER.debug("Message Body:[{}]", new String(body));
//        int topicLentgh = mappedByteBuffer.get();
//        LOGGER.debug("Message TopicLentgh:[{}]", topicLentgh);
//        byte[] topic = new byte[topicLentgh];
//        mappedByteBuffer.get(topic);
//        LOGGER.debug("Message Topic:[{}]", new String(topic));
//        int propertiesLength = mappedByteBuffer.getShort();
//        LOGGER.debug("Message PropertiesLength:[{}]", propertiesLength);
//        byte[] properties = new byte[propertiesLength];
//        mappedByteBuffer.get(properties);
//        LOGGER.debug("Message Properties:[{}]", new String(properties));
//    }
//
//    public static class Tag {
//        public long offset;
//        public long size;
//        public long code;
//
//        public Tag(long offset, long size, long code) {
//            this.offset = offset;
//            this.size = size;
//            this.code = code;
//        }
//
//        @Override
//        public String toString() {
//            return "Tag{" +
//                    "offset=" + offset +
//                    ", size=" + size +
//                    ", code=" + code +
//                    '}';
//        }
//    }
//}
