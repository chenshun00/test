//package top.huzhurong.gateway.test.view;
//
//import com.alibaba.fastjson.JSONObject;
//import com.taobao.metamorphosis.Message;
//import com.taobao.metamorphosis.client.MessageSessionFactory;
//import com.taobao.metamorphosis.client.MetaClientConfig;
//import com.taobao.metamorphosis.client.MetaMessageSessionFactory;
//import com.taobao.metamorphosis.client.consumer.ConsumerConfig;
//import com.taobao.metamorphosis.client.consumer.MessageConsumer;
//import com.taobao.metamorphosis.client.consumer.MessageListener;
//import com.taobao.metamorphosis.exception.MetaClientException;
//import com.taobao.metamorphosis.utils.ZkUtils;
//
//import java.util.concurrent.Executor;
//
///**
// * @author chenshun00
// * @since 2019/12/13 11:11 AM
// */
//public class ConsumerX {
//    public static void main(String[] args) throws MetaClientException {
//        final String group = "here-test";
//        ConsumerConfig consumerConfig = new ConsumerConfig(group);
//        final MetaClientConfig metaClientConfig = new MetaClientConfig();
//        final ZkUtils.ZKConfig zkConfig = new ZkUtils.ZKConfig();
//        zkConfig.zkConnect = "192.168.63.127:2181";
//        metaClientConfig.setZkConfig(zkConfig);
//        MessageSessionFactory sessionFactory = new MetaMessageSessionFactory(metaClientConfig);
//        final MessageConsumer consumer = sessionFactory.createConsumer(consumerConfig);
//        consumer.subscribe("chenshun00", 1, new MessageListener() {
//            @Override
//            public void recieveMessages(Message message) throws InterruptedException {
//                System.out.println(JSONObject.toJSONString(message));
//            }
//
//            @Override
//            public Executor getExecutor() {
//                return null;
//            }
//        });
//        consumer.completeSubscribe();
//    }
//}
