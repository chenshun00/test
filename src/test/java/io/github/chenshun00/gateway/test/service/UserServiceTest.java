package io.github.chenshun00.gateway.test.service;

import io.github.chenshun00.gateway.test.dao.TestMockDao;
import io.github.chenshun00.gateway.test.dao.UserDao;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@SpringBootTest
@RunWith(SpringRunner.class)
@ActiveProfiles("local")
public class UserServiceTest {

    @InjectMocks
    private UserService userService = new UserService();

    @Mock
    private UserDao userDao;
    @Mock
    private TestMockDao testMockDao;

    @Before
    public void setUp() throws Exception {
        // 初始化测试用例类中由Mockito的注解标注的所有模拟对象
        MockitoAnnotations.initMocks(this);
    }

    /**
     * 被测类中被@Autowired 或 @Resource 注解标注的依赖对象，如何控制其返回值
     */
    @Test
    public void result() {
        when(userDao.insert()).thenReturn("陈顺");
        when(testMockDao.good()).thenReturn("2234");
        User user = new User();
        user.setUsername("fff");
        String result = userService.result(user);
        assertEquals("陈顺_2234", result);
    }


    /**
     * 被测函数A调用被测类其他函数B，怎么控制函数B的返回值？
     */
    @Test
    public void testOtherReturn() {
        when(userDao.insert()).thenReturn("陈顺");
        when(testMockDao.good()).thenReturn("2234");
        User user = new User();
        user.setUsername("fff");
        userService = spy(userService);
        //doReturn 开头
        doReturn(Boolean.FALSE).when(userService).otherUsage();
        String result = userService.result(user);
        assertEquals("陈顺_2234", result);
    }

    @Test
    public void testOtherReturn2() {
        when(userDao.insert()).thenReturn("陈顺");
        when(testMockDao.good()).thenReturn("2234");
        User user = new User();
        user.setUsername("fff");
        userService = spy(userService);
        //doReturn 开头
        doReturn(Boolean.FALSE).when(userService).otherUsage();
        when(userService.otherUsage()).thenReturn(Boolean.FALSE);
        String result = userService.result(user);
        assertEquals("陈顺_2234", result);
    }
}