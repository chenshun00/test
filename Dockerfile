# FROM 指定基础镜像
FROM registry.cn-zhangjiakou.aliyuncs.com/jstopen/springboot2:1.0.0
MAINTAINER chenshun chenshun00@gmail.com
ADD target/test_web.jar test_web.jar
# 必要命令 ipconfig telnet
# 不要使用 EXPOSE 命令
ENTRYPOINT [ "sh", "-c", "java -Djava.security.egd=file:/dev/./urandom -jar test_web.jar"]